package com.zuitt;

import java.io.IOException;

import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

public class UserServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7560556849380195433L;

	public void init() throws ServletException{

		System.out.println("********************************");
		System.out.println("UserServlet has been initialized");
		System.out.println("********************************");
	}
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException{
		//System properties firstname
		System.getProperties().put("firstname",req.getParameter("firstname"));
		
		//Lastname httpSession
		HttpSession session = req.getSession();
		session.setAttribute("lastname",req.getParameter("lastname"));
		
		//Email Servlet Context setAttribute method
		ServletContext srvContext = getServletContext();
		srvContext.setAttribute("email", req.getParameter("email"));
		
		res.sendRedirect("details?contact="+req.getParameter("contact"));
		
	}

	public void destroy(){
		System.out.println("********************************");
		System.out.println("UserServlet has been destroyed.");
		System.out.println("********************************");
	}

}
